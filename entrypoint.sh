#!/bin/sh

# run file and return failure if something fails
set -e

# populate template with environment variables
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# start nginx service
nginx -g 'daemon off;'